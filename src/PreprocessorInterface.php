<?php

namespace Drupal\d3_sankey;

/**
 * An interface for objects that transform other data into raw Sankey data.
 *
 * The raw node and link data used by the Sankey chart plugin for the D3 library
 * is very closely related: if you add, rearrange, or delete nodes, you also
 * have to change all the links, and vice-versa.
 *
 * This close inter-relatedness makes the data difficult to work with. It would
 * be useful to be able to create some sort of preprocessor to turn data that is
 * easier to work with into the raw data that the Sankey chart plugin for the D3
 * library needs.
 *
 * This interface provides a basic template for a preprocessor. It purposely
 * does not include any templates for functions to input data into the
 * preprocessor: those could vary widely depending on the data that needs to be
 * preprocessed (e.g.: some data might be in a tabular form, other data might be
 * linked-lists, etc.).
 */
interface PreprocessorInterface {

  /**
   * Get the raw data that makes up this Sankey diagram.
   *
   * @return \Drupal\d3_sankey\Model\RawSankeyData
   *   The raw data that makes up this Sankey diagram.
   */
  public function getRawData();

}

<?php

namespace Drupal\d3_sankey\Model;

use Drupal\d3_sankey\DrupalCoreAdapter;

/**
 * An object representing a single, raw Sankey node.
 */
class Node {

  /**
   * A mockable wrapper around D7 procedural functions.
   *
   * @var \Drupal\d3_sankey\DrupalCoreAdapter
   */
  public $adapter;

  /**
   * A label for the node.
   *
   * @var string
   */
  public $name;

  /**
   * An SVG ID attribute (similar to an HTML ID attribute) to add to the node.
   *
   * @var string
   */
  public $id;

  /**
   * D3SankeyNode constructor.
   *
   * @param string $name
   *   A label for the node.
   * @param string|null $id
   *   An SVG ID attribute (similar to an HTML ID attribute) to add to the node.
   * @param DrupalCoreAdapter $adapter
   *   A mockable wrapper around D7 procedural functions.
   */
  public function __construct($name, $id = NULL, DrupalCoreAdapter $adapter = NULL) {
    $this->adapter = ($adapter) ? $adapter : new DrupalCoreAdapter();

    $this->name = (string) $name;
    $this->id = ($id) ? (string) $id : $this->adapter->drupalHtmlId($name);
  }

  /**
   * Get an associated-array version of this node.
   *
   * @return array
   *   An associative array containing:
   *   - name: A label for the node.
   *   - id (optional): An SVG ID attribute (similar to an HTML ID attribute) to
   *     add to the node.
   */
  public function toAssocArray() {
    $answer = array('name' => $this->name);

    if (!is_null($this->id)) {
      $answer['id'] = $this->id;
    }

    return $answer;
  }

}

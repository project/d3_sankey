/**
 * @file
 * D3 Sankey library.
 */

(function ($, Drupal, d3) {

  'use strict';

  Drupal.d3.sankey = function (select, settings) {
    var chartCanvas;
    var chart;
    var colorNodes;
    var colorLinks;

    // Grab settings from the settings object passed to us by the D3 module. Use
    // defaults if the settings we want do not exist in the settings object.
    var sankeyType = (settings.sankeyType || 'Sankey');
    var width = parseInt(settings.width || 700);
    var height = parseInt(settings.height || 400);
    var nodeWidth = parseInt(settings.nodeWidth || 24);
    var nodePadding = parseInt(settings.nodePadding || 8);
    var spread = Boolean(settings.spread || true);
    var iterations = parseInt(settings.iterations || 1);
    var alignLabel = (settings.alignLabel || 'auto');

    // Grab data for the chart if it exists in it's raw form; if not, use empty
    // arrays for now.
    var nodes = (settings.nodes || []);
    var links = (settings.links || []);

    // If we were passed an array of node colors, create an ordinal scale and
    // use that.
    if (Array.isArray(settings.node_colors)) {
      colorNodes = d3.scale.ordinal().range(settings.node_colors);
    }
    // If we were passed a single node color, use that.
    else if (typeof settings.node_colors === 'string') {
      colorNodes = settings.node_colors;
    }
    // If we were passed a function, use that.
    else if (typeof settings.node_colors === 'function') {
      colorNodes = settings.node_colors;
    }

    // If we were passed an array of link colors, create an ordinal scale and
    // use that.
    if (Array.isArray(settings.link_colors)) {
      colorLinks = d3.scale.ordinal().range(settings.link_colors);
    }
    // If we were passed a single link color, use that.
    else if (typeof settings.link_colors === 'string') {
      colorLinks = settings.link_colors;
    }
    // If we were passed a function, use that.
    else if (typeof settings.link_colors === 'function') {
      colorLinks = settings.link_colors;
    }

    // In the chart element, add an SVG tag with the correct classes, width, and
    // height; and inside that, a group for the chart itself.
    chartCanvas = d3.select('#' + settings.id)
      .append('svg').attr('class', 'sankey').attr('width', width).attr('height', height);

    // Set up the chart.
    chart = chartCanvas.chart(sankeyType);
    chart.nodeWidth(nodeWidth)
      .nodePadding(nodePadding)
      .iterations(iterations)
      .spread(spread)
      .alignLabel(alignLabel);

    // If we were given node colors, use them.
    if (typeof colorNodes !== 'undefined') {
      chart.colorNodes(colorNodes);
    }

    // If we were given link colors, use them.
    if (typeof colorLinks !== 'undefined') {
      chart.colorLinks(colorLinks);
    }

    // Draw a sankey chart with the data.
    chart.draw({nodes: nodes, links: links});
  };

})(jQuery, Drupal, d3);
